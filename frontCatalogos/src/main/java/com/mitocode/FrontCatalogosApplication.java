package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontCatalogosApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontCatalogosApplication.class, args);
	}

}
