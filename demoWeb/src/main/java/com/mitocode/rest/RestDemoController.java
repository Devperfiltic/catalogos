package com.mitocode.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mitocode.repo.IProductoRepo;
import com.mitocode.model.producto;

@RestController
@RequestMapping("/producto")
public class RestDemoController {
	
	@Autowired
	private IProductoRepo repo;
	
	@GetMapping
	public List<producto> listar(){
		//funcion para listar
		return repo.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public Optional<producto> listar_sub(@PathVariable Integer id){
		//funcion para listar
		return repo.findById(id);
	}
	
	@PostMapping
	public void insert(@RequestBody producto pro){
		//funcion insertar
		repo.save(pro);
	}
}
